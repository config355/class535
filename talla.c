#include <stdio.h>
#include "cs50.h"

void mostrar_verbosidad();
float de_kilos_a_libras(float kgs);
float de_cms_a_pulgadas(float centimetros);
float calcular_tam_sombrero(float libras, float pulgadas);

int main(){
    float peso;
    float estatura;
    float libras;
    float pulgadas;
    float sombrero_tamano;
    char verbosidad;

    peso = get_float("peso (kg) : ");
    estatura = get_float("estatura (cms): ");
    verbosidad = get_char("verbosidad? [s]si/[n]no: ");

    libras = de_kilos_a_libras(peso);
    printf("libras: %f\n", libras);

    pulgadas = de_cms_a_pulgadas(estatura);
    sombrero_tamano = calcular_tam_sombrero(libras, pulgadas);

    if ( verbosidad == 's' ){
        mostrar_verbosidad();
    }

    printf("talla del sombrero: %.1f", sombrero_tamano);

    return 0;
}

void mostrar_verbosidad(){
    printf("\n");
    printf("abriendo navegador..............................ok\n");
    printf("ingresando a google.com.........................ok\n");
    printf("consultando cuantas libras hay en un kilo.......ok\n");
    printf("calculando libras...............................ok\n");
    printf("consultando cuantas pulgadas hay en cms.........ok\n");
    printf("calculando pulgadas.............................ok\n");
    printf("\n");
}

float de_kilos_a_libras(float kgs){
    float libras = kgs * 2.2;

    return libras;
}

float de_cms_a_pulgadas(float centimetros){
    float pulgadas;
    pulgadas = centimetros * 0.39;
    return pulgadas;
}

float calcular_tam_sombrero(float libras, float pulgadas){
    float tamanio ;
    tamanio = (libras / pulgadas) * 2.9;

    return tamanio;
}