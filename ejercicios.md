# ejercicios de programacion #


Escriba un programa que lea 10 números y que despliegue la suma de todos los números mayores a cero,
la suma de todos los números menores que cero (la cual será un número menor o igual a cero) y la suma
de todos los números, ya sean positivos, negativos o cero. El usuario debe introducir diez números una a uno
y los puede introducir en cualquier orden.

```
#include <stdio.h>
#include "cs50.h"

int main(){

    // crear el array
    float numeros[10] = {12, -64, 24, -4, -2, 5, 9, -3, 2, 40};

    //pedir al usuario los numeros
    for (int i =0 ; i < 10; i++){
        numeros[i] = get_float("elemento %i: ", i+1);
    }

    //recorrer cada uno de los elementos
    float suma = 0;
    for(int i=0; i<10; i++){
        if ( numeros[i]> 0 ){
            // agregar a la suma
            suma += numeros[i];
        }
    }

    // imprimir la suma
    printf("la suma de los numeros positivos es: %.2f\n", suma);

    return 0;
}
```

Modifique el programa anterio de modo que despliegue la suma de todos los
números positivos, el promedio de todos los números positivos, la suma de todos los números negativos,
el promedio de todos los números negativos, la suma de todos los números negativos y el promedio de
todos los números introducidos.


 Escriba un programa que pida la estatura, el peso y la edad del usuario y luego calcule las tallas de ropa según las fórmulas:

 - Tamaño de sombrero = peso en libras dividido entre estatura en pulgadas y el resultado multiplicado por
2.9

- Tamaño de saco (pecho en pulgadas) = altura multiplicada por peso y dividida entre 288.  

- Cintura en pulgadas = peso dividido entre 5.7.  
