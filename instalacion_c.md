# scoop: programas para la clase #

instalar los  programas
    
    scoop install git
    scoop bucket add extras
    scoop bucket add versions
    scoop install make mingw-winlibs-llvm neovim vscodium windows-terminal

abrir vscodium y `ctrl+Shift+x` instalar la extension `Clang-Format` 

y configurar el editor con la siguiente opción en `settings.json`

    {
        "editor.formatOnSave": true
    }

# configurar el vscodium para que formatee al guardar #

presione `ctrl + shift + p`, escribir en `open user settings.json` y colocar
    {
        "editor.formatOnSave": true
    }

# importar cs50 #

descargar cs50.c

    curl.exe --remote-name https://raw.githubusercontent.com/cs50/libcs50/main/src/cs50.c

descargar cs50.h

    curl.exe --remote-name https://raw.githubusercontent.com/cs50/libcs50/main/src/cs50.h 

# descargar MAKEFILE #

    curl.exe --remote-name https://gitlab.com/config355/class535/-/raw/main/makefile



(Opcional) instalar con json

descargar la configuracion

    curl.exe --remote-name https://gitlab.com/config355/class535/-/raw/main/scoop_algoritmos.json


importa a scoop 

    scoop import scoop_algoritmos.json


