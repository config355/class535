hello.exe: hello.c cs50.c
	clang -Wno-deprecated-declarations -o hello.exe hello.c cs50.c

%.exe: %.c cs50.c
	clang -o $@ $< cs50.c 

clean:
	del *.exe
